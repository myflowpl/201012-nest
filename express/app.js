const express = require('express')
const app = express()

// app.get('/api/user/sum', function (req, res) { res.send('hello world') })
// app.get('/api/user/:id', function (req, res) { res.send('hello world') })

app.get('/user', 

  function (req, res, next) {    // logger middleware
    console.log(req.url);
    next();
  },

  function (req, res, next) {    // auth middleware
    req.user = req.query.name;
    console.log('QUERY', req.query)
    next();
  },

  function (req, res, next) {    // guard middleware
    (req.user) ? next() : next('forbidden');
  },

  function (req, res) {          // request handler
    res.send('Hi ' + req.user)
  },

  function (err, req, res, next) {    // error handler
    res.json({status: 500, err})
  },
)

exports.app = app;
